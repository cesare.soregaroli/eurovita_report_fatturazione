﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Fatturazione_Mail
{
    public class Fatturazione_Mail
    {

        public System.String email_smtpserver { get; set; }
        public System.String email_sendemail { get; set; }
        public System.String email_from { get; set; }
        public System.String email_to { get; set; }
        public System.String email_cc { get; set; }
        public System.String email_eccezione_to { get; set; }
        public System.String email_path_folder { get; set; }
        public System.String email_name_file { get; set; }
        
        public void SendMailError(System.Exception ex,Boolean _microframe)
        {
            MailMessage message= null;
            SmtpClient client = null;
            try
            {
                if (_microframe)
                {
                    message = new MailMessage(this.email_from, this.email_to);
                    message.CC.Add(email_cc);
                }
                else
                {
                    message = new MailMessage(this.email_from, this.email_eccezione_to);
                }
                message.Subject = $"Eurovita Report Fatturazione : {ex.Message}";
                if (_microframe)
                {
                    message.Body = preparataTestoMail(ex).ToString();
                }
                else
                {
                    message.Body = preparataTestoMailStackTrace(ex).ToString();
                }
                client = new SmtpClient(this.email_smtpserver);
                client.UseDefaultCredentials = true;
                client.Send(message);
            }
            catch (Exception)
            {
                throw;
            }
            finally {
                if (message != null) { message.Dispose(); }
                if (client != null) { client.Dispose(); }
            }
        }

        public void SendMailAttachment()
        {
            MailMessage message = null;
            SmtpClient client = null;
            try
            {
                message = new MailMessage(this.email_from, this.email_to);
                message.CC.Add(email_cc);                
                message.Subject = $"Eurovita Report Fatturazione : {this.email_name_file}";
                message.Body = $"Buongiorno,{Environment.NewLine}In allegato alla presente trovare il report di fatturazione di Eurovita.{Environment.NewLine}Grazie.";
                message.Attachments.Add(new Attachment(System.IO.Path.Combine(this.email_path_folder,this.email_name_file)));
                client = new SmtpClient(this.email_smtpserver);
                client.UseDefaultCredentials = true;
                client.Send(message);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (message != null) { message.Dispose(); }
                if (client != null) { client.Dispose(); }
            }
        }

        private System.Text.StringBuilder preparataTestoMail(System.Exception ex)
        {
            StringBuilder strMail = new StringBuilder();
            try
            {
                strMail.Append($"{System.Environment.NewLine}{System.Environment.NewLine} Dati ticket: {System.Environment.NewLine}asset: Eurovita_Report_Fatturazione{System.Environment.NewLine}");         
                strMail.Append($"Errore Generato : {ex.Message}");
            }
            catch (Exception)
            {
                throw;
            }
            return strMail;
        }

        private System.Text.StringBuilder preparataTestoMailStackTrace(System.Exception ex)
        {
            StringBuilder strMail = new StringBuilder();
            try
            {                
                strMail.Append($"Errore Generato: {ex.Message}{System.Environment.NewLine}");
                strMail.Append($"Stack Trace: {ex.StackTrace}");
            }
            catch (Exception)
            {
                throw;
            }
            return strMail;
        }



    }
}
