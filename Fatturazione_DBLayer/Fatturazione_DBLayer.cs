﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Fatturazione_DB.Dev;
using System.Globalization;

namespace Fatturazione_DB
{
    public class Fatturazione_DBLayer
    {

        private DateTime _Date_From;
        public DateTime Date_From
        {
            get => _Date_From;
            set
            {
                if (value != null)
                {
                    _Date_From = value;
                }
                else { throw new Exception("Data di Inizio Ricerca deve essere valorizzata."); }
            }
        }

        private DateTime _Date_To;
        public DateTime Date_To
        {
            get => _Date_To;
            set
            {
                if (value != null)
                {
                    _Date_To = value;
                }
                else { throw new Exception("Data di Fine Ricerca deve essere valorizzata."); }
            }
        }

        private System.String _ConnString;
        public System.String ConnString
        {
            get => _ConnString;
            set
            {
                if (value != null)
                {
                    _ConnString = value;
                }
                else { throw new Exception("Connessione al Databaser deve essere valorizzata."); }
            }
        }

        private System.String _fileName_In;
        public System.String fileName_In
        {
            get => _fileName_In;
            set
            {
                if (value != null)
                {
                    _fileName_In = value;
                }
                else { throw new Exception("Nome del file di input non valorizzato."); }
            }
        }

        private System.String _fileName_Out;
        public System.String fileName_Out
        {
            get => _fileName_Out;
            set
            {
                if (value != null && !string.IsNullOrEmpty(value))
                {
                    _fileName_Out = value;
                }
                else { throw new Exception("Nome del file di output non valorizzato."); }
            }
        }

        public System.String Folder_Out { get; set; }
        public System.String Folder_Backup { get; set; }

        public void Create_Report_ToConsole()
        {
            try
            {
                Create_Report();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Create_Report_ToWPF()
        {
            try
            {
                Create_Report();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void Create_Report()
        {
            System.Data.DataTable _dt = null;
            DevExpressExcel oDevExpressWord = new DevExpressExcel();
            try
            {
                _dt = Read_Report_File();
                if (_dt != null)
                {
                    
                    oDevExpressWord.loadDocument(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Report", fileName_In).ToString());
                    addMonthToSheet(oDevExpressWord);
                    Load_Sheet_Data(oDevExpressWord, _dt);
                    Replicate_Sheet_Rule(oDevExpressWord);
                    //Filter_AddSheet(oDevExpressWord);
                    load_pivot_table(oDevExpressWord);                    


                    if (!new System.IO.DirectoryInfo(this.Folder_Out).Exists) { System.IO.Directory.CreateDirectory(this.Folder_Out); }
                    if (new System.IO.FileInfo(System.IO.Path.Combine(this.Folder_Out, this.fileName_Out)).Exists) {
                        System.IO.File.Delete(System.IO.Path.Combine(this.Folder_Out, this.fileName_Out));
                    }
                    oDevExpressWord.SaveDocument(System.IO.Path.Combine(this.Folder_Out, this.fileName_Out).ToString());

                    if (!new System.IO.DirectoryInfo(this.Folder_Backup).Exists) { System.IO.Directory.CreateDirectory(this.Folder_Backup); }
                    if (new System.IO.FileInfo(System.IO.Path.Combine(this.Folder_Backup, this.fileName_Out)).Exists)
                    {
                        System.IO.File.Delete(System.IO.Path.Combine(this.Folder_Backup, this.fileName_Out));
                    }
                    oDevExpressWord.SaveDocument(System.IO.Path.Combine(this.Folder_Backup, this.fileName_Out).ToString());

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void addMonthToSheet(DevExpressExcel _DevExpressExcel) {
            System.String _mese = string.Empty;
            try
            {
                DateTimeFormatInfo dtTimeFormtInfo = CultureInfo.GetCultureInfo("it-IT").DateTimeFormat;
                _mese = dtTimeFormtInfo.GetMonthName(this.Date_To.Month);
                _DevExpressExcel.AddTextInCell("Fatt_Servizi Logistici", _mese, "F", "4");

                
                _DevExpressExcel.AddTextInCell("Fatt_Servizi Logistici", this.Date_To.Year.ToString(), "B", "4");
            }
            catch (Exception ex)
            {
                throw ex;
            }        
        }


        private void Load_Sheet_Data(DevExpressExcel _DevExpressExcel, System.Data.DataTable _dt)
        {
            List<ItemSheetList> _ListSheet = new List<ItemSheetList> { new ItemSheetList() { _SheetName = "Elenco Documenti Portafoglio", _TypeExtraction = "1" }, new ItemSheetList() { _SheetName = "Elenco Documenti No Portafoglio", _TypeExtraction = "0" }, new ItemSheetList { _SheetName = "Distinte", _TypeExtraction = "D" } };
            System.Data.DataTable _dtFiltered = null;
            try
            {
                if (_dt != null && _dt.Rows.Count > 0)
                {
                    foreach (var _sheet in _ListSheet)
                    {
                        _dtFiltered = filteredTable(_dt, _sheet._TypeExtraction);
                        if (_dtFiltered != null && _dtFiltered.Rows.Count > 0)
                        {
                            _DevExpressExcel.AddTextInCell(_sheet._SheetName, _dtFiltered, "A", "1");
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void load_pivot_table(DevExpressExcel _DevExpressExcel)
        {
            List<ItemSheetList> _ListSheet = new List<ItemSheetList> { new ItemSheetList() { _SheetName = "Conteggio doc portaf",  _SheetSource = "Elenco Documenti Portafoglio", _NamePivot = "Pivot_Conteggio_Doc_Portaf" },
                                                                      new ItemSheetList() { _SheetName = "Conteggio doc no portaf", _SheetSource="Elenco Documenti No Portafoglio" , _NamePivot= "Pivot_Conteggio_Doc_No_Portaf"},
                                                                      new ItemSheetList() { _SheetName = "Conteggio distinte", _SheetSource= "Distinte", _NamePivot="Pivot_Distinte"},
                                                                      new ItemSheetList() { _SheetName = "Conteggio Fogli", _SheetSource = "Elenco Documenti Portafoglio" , _NamePivot = "Pivot_Cont_Fogli_1" },
                                                                      new ItemSheetList() { _SheetName = "Conteggio Fogli", _SheetSource="Elenco Documenti No Portafoglio" , _NamePivot= "Pivot_Cont_Fogli_2"},
                                                                      new ItemSheetList() { _SheetName = "Conteggio Fogli", _SheetSource= "Distinte", _NamePivot="Pivot_Cont_Fogli_3"},
                                                                      new ItemSheetList() { _SheetName = "Verifica Originalità", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Verifica"},
                                                                      new ItemSheetList() { _SheetName = "Scansione documenti in genere", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_DocINGenere_1"},
                                                                      new ItemSheetList() { _SheetName = "Scansione documenti in genere", _SheetSource= "Elenco Documenti No Portafoglio", _NamePivot="Pivot_DocINGenere_2"},
                                                                      new ItemSheetList() { _SheetName = "Scansione documenti in genere", _SheetSource= "Distinte", _NamePivot="Pivot_DocINGenere_3"},
                                                                      new ItemSheetList() { _SheetName = "Scansione documenti sanitari", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Doc_Sanitari"},
                                                                      new ItemSheetList() { _SheetName = "Scansione doc carta chimica", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Carta_Chimica"},
                                                                      new ItemSheetList() { _SheetName = "Tipizzazione", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_tipizzazione_1"},
                                                                      new ItemSheetList() { _SheetName = "Tipizzazione", _SheetSource= "Elenco Documenti No Portafoglio", _NamePivot="Pivot_tipizzazione_2"},
                                                                      new ItemSheetList() { _SheetName = "Tipizzazione", _SheetSource= "Distinte", _NamePivot="Pivot_tipizzazione_3"},
                                                                      new ItemSheetList() { _SheetName = "Indicizzazione prima pagina", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Prima_Pagina_1"},
                                                                      new ItemSheetList() { _SheetName = "Indicizzazione prima pagina", _SheetSource= "Elenco Documenti No Portafoglio", _NamePivot="Pivot_Prima_Pagina_2"},
                                                                      new ItemSheetList() { _SheetName = "Indicizzazione prima pagina", _SheetSource= "Distinte", _NamePivot="Pivot_Prima_Pagina_3"},
                                                                      new ItemSheetList() { _SheetName = "Index - pagine seguenti", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Index_PagSeg_1"},
                                                                      new ItemSheetList() { _SheetName = "Index - pagine seguenti", _SheetSource= "Elenco Documenti No Portafoglio", _NamePivot="Pivot_Index_PagSeg_2"},
                                                                      new ItemSheetList() { _SheetName = "Index - pagine seguenti", _SheetSource= "Distinte", _NamePivot="Pivot_Index_PagSeg_3"},
                                                                      new ItemSheetList() { _SheetName = "Validazione pratiche assuntive", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Pratiche_Assuntive"},
                                                                      new ItemSheetList() { _SheetName = "Validazione pratiche altri doc", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Pratiche_Validazione"},
                                                                      new ItemSheetList() { _SheetName = "Completamento", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Completamento"},
                                                                      //new ItemSheetList() { _SheetName = "Rilavorazioni", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Rilavorazioni"},
                                                                      new ItemSheetList() { _SheetName = "Documenti elettronici", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_DocElettronici"},
                                                                      new ItemSheetList() { _SheetName = "Documenti senza area", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_DocSenzaArea"},
                                                                      new ItemSheetList() { _SheetName = "PIVOT Riassuntiva", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Riassunto_1"},
                                                                      new ItemSheetList() { _SheetName = "PIVOT Riassuntiva", _SheetSource= "Distinte", _NamePivot="Pivot_Riassunto_2"},
                                                                      new ItemSheetList() { _SheetName = "PIVOT Riassuntiva", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Riassunto_3"},
                                                                      new ItemSheetList() { _SheetName = "PIVOT Riassuntiva", _SheetSource= "Elenco Documenti No Portafoglio", _NamePivot="Pivot_Riassunto_4"},
                                                                      new ItemSheetList() { _SheetName = "PIVOT Riassuntiva", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Riassunto_5"},
                                                                      new ItemSheetList() { _SheetName = "PIVOT Riassuntiva", _SheetSource= "Elenco Documenti Portafoglio", _NamePivot="Pivot_Riassunto_6"} }; 
            
            DevExpress.Spreadsheet.Range _range = null;
            try
            {
                foreach (var _sheetItem in _ListSheet)
                {
                    _range = _DevExpressExcel.Range_UsedSheet(_sheetItem._SheetSource);

                    if (_range != null)
                    {
                        _DevExpressExcel.ChangePivotRange(_sheetItem._SheetName, _sheetItem._SheetSource, _sheetItem._NamePivot, _range);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void Filter_AddSheet(DevExpressExcel _DevExpressExcel)
        {
            List<ItemSheetList> _ListSheet = new List<ItemSheetList> { new ItemSheetList() { _SheetName = "Elenco Documenti Portafoglio", _TypeExtraction = "1" }, new ItemSheetList() { _SheetName = "Elenco Documenti No Portafoglio", _TypeExtraction = "0" }, new ItemSheetList { _SheetName = "Distinte", _TypeExtraction = "D" } };
            DevExpress.Spreadsheet.Range _range = null;
            try
            {
                foreach (var _sheetItem in _ListSheet)
                {
                    _range = _DevExpressExcel.Range_UsedSheet(_sheetItem._SheetName);

                    if (_range != null)
                    {
                        _DevExpressExcel.Filte_Add(_sheetItem._SheetName, _range);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void Replicate_Sheet_Rule(DevExpressExcel _DevExpressExcel)
        {
            List<ItemSheetList> _ListSheet = new List<ItemSheetList> { new ItemSheetList() { _SheetName = "Elenco Documenti Portafoglio", _TypeExtraction = "1" }, new ItemSheetList() { _SheetName = "Elenco Documenti No Portafoglio", _TypeExtraction = "0" }, new ItemSheetList { _SheetName = "Distinte", _TypeExtraction = "D" } };
            DevExpress.Spreadsheet.Range _range = null;
            String _colonna = String.Empty;
            try
            {
                foreach (var _sheetItem in _ListSheet)
                {
                    _range = _DevExpressExcel.Range_UsedSheet(_sheetItem._SheetName);

                    if (_range != null)
                    {

                        _DevExpressExcel.Replicate_Rule(_sheetItem._SheetName, "W", 1, _range.RowCount);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private DataTable filteredTable(System.Data.DataTable _dtTofilter, System.String _portafoglio)
        {
            DataTable _dtTable = null;
            DataRow[] _drTable = null;
            try
            {
                _drTable = _dtTofilter.Select(String.Concat("Portafoglio = '", _portafoglio, "'"));//.CopyToDataTable();
                if (_drTable != null)
                {
                    _dtTable = _drTable.CopyToDataTable();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _dtTable;
        }

        private System.Data.DataTable Read_Report_File()
        {
            System.Data.DataTable _dt = new DataTable();
            try
            {
                string query = "select * from VIEW_FATTURAZIONE_PER_EXCEL WHERE convert(date,[data archiviazione],103) >= convert(date,@DATE_FROM,103) and convert(date,[data archiviazione],103) <= convert(date,@DATE_TO,103) and canale not in('IDM - PREGRESSO','IDM','IDM - PREGRESSO NO PTF.') ORDER BY Portafoglio ASC,[DATA Archiviazione] ASC ";

                SqlConnection conn = new SqlConnection(ConnString);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.Add(new SqlParameter("DATE_FROM", SqlDbType.DateTime));
                cmd.Parameters["DATE_FROM"].Value = _Date_From;
                cmd.Parameters.Add(new SqlParameter("DATE_TO", SqlDbType.DateTime));
                cmd.Parameters["DATE_TO"].Value = _Date_To;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(_dt);
                conn.Close();
                da.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
            return _dt;
        }
    }

    class ItemSheetList
    {

        public String _SheetName { get; set; }
        public String _TypeExtraction { get; set; }

        public String _SheetSource { get; set; }
        public String _NamePivot { get; set; }


    }

}
