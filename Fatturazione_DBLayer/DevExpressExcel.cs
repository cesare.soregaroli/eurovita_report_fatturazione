﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Spreadsheet;
using DevExpress.XtraPrinting;
using DevExpress.XtraSpreadsheet.Services;
using DevExpress.XtraSpreadsheet.Services.Implementation;
using System;

namespace Fatturazione_DB.Dev
{
        public class DevExpressExcel : System.IDisposable
    {
            bool disposed = false;
            DevExpress.Spreadsheet.Workbook _server = null;

            public DevExpressExcel()
            {
                _server = new Workbook();
            }

            public void CreateDocument(System.String _path)
            {
                try
                {
                    _server.CreateNewDocument();                
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public void loadDocument(System.String _path)
            {
                try
                {
                    _server.LoadDocument(_path, DocumentFormat.OpenXml);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public void AddSheetToDocument(System.String _nameSheet)
            {
                try
                {
                    _server.Worksheets.Add().Name = _nameSheet;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            public void RenameSheet(String nomeOld, String nomeNew)
            {
                try
                {
                    List<Worksheet> _worksheet = _server.Worksheets.ToList();
                    foreach (Worksheet _tmpworksheet in _worksheet)
                    {
                        if (_tmpworksheet.Name == nomeOld)
                        {
                            _tmpworksheet.Name = nomeNew;
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            public void RenameSheet(int sheetPos, String nomeNew)
            {
                try
                {
                    List<Worksheet> _worksheet = _server.Worksheets.ToList();
                    foreach (Worksheet _tmpworksheet in _worksheet)
                    {
                        if (_tmpworksheet.Index == sheetPos)
                        {
                            _tmpworksheet.Name = nomeNew;
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            public void AddTextInCell(System.String _nameSheet, System.String _value)
            {
                try
                {
                    Worksheet worksheet = _server.Worksheets[_nameSheet];
                    worksheet.Cells["A1"].SetValueFromText(_value);
                }
                catch (Exception)
                {
                    throw;
                }
            }


            public void AddTextInCell(System.String _nameSheet, System.Data.DataTable _valuedt)
            {
                try
                {
                    Worksheet worksheet = _server.Worksheets[_nameSheet];                
                    worksheet.Import(_valuedt, true, 0, 0);
                }
                catch (Exception)
                {
                    throw;
                }
            }

        public void AddTextInCell(System.String _nameSheet, System.Data.DataTable _valuedt,System.String _colonna,System.String _riga)
        {
            Int32 _indiceRiga = 0;
            Int32 _indiceColonna = 0;
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                _indiceRiga = worksheet.Rows[_riga].Index;
                _indiceColonna = worksheet.Columns[_colonna].Index;
                worksheet.Import(_valuedt, true, _indiceRiga, _indiceColonna);               
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AddTextInCell(System.String _nameSheet, System.String _value, System.String _colonna, System.String _riga)
        {
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                worksheet.Cells[$"{_colonna}{_riga}"].SetValueFromText(_value);                
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ChangePivotRange(System.String _nameSheetPivot,System.String _nameSheetSource, System.String _namePivotTable,Range _range)
        {
            
            try
            {

                Worksheet sourceWorksheet = _server.Worksheets[_nameSheetSource];

                Worksheet worksheet = _server.Worksheets[_nameSheetPivot];
                
                // Access the pivot table by its name in the collection. 
                PivotTable pivotTable = worksheet.PivotTables[_namePivotTable];

                pivotTable.ChangeDataSource(_range);
                
                pivotTable.Cache.Refresh();

            }
            catch (Exception)
            {
                throw;
            }
        }


        public Range Range_UsedSheet(System.String _nameSheet)
        {
            Range _Range;            
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];               
                _Range = worksheet.GetUsedRange();
            }
            catch (Exception)
            {
                throw;
            }
            return _Range;
        }


        public void Replicate_Rule(System.String _nameSheet,System.String _columnToCopy,Int32 _RowToCopy,Int32 _RowMaxCopy)
        {
            System.String _formula;
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                _formula = worksheet.Columns[_columnToCopy][_RowToCopy].Formula;
                worksheet.Range[$"{_columnToCopy}2:{_columnToCopy}{_RowMaxCopy}"].Formula = _formula;
            }
            catch (Exception)
            {
                throw;
            }            
        }

        public void Filte_Add(System.String _nameSheet, Range _range)
        {            
            try
            {
                Worksheet worksheet = _server.Worksheets[_nameSheet];
                worksheet.AutoFilter.Apply(_range);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<System.String> ExportSheetName()
            {
                List<System.String> _ListnameSheet = new List<string> { };
                try
                {
                    List<Worksheet> _worksheet = _server.Worksheets.ToList();
                    foreach (Worksheet _tmpworksheet in _worksheet)
                    {
                        _ListnameSheet.Add(_tmpworksheet.Name);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                return _ListnameSheet;
            }

            public System.String ExportValueCell(System.String _nameSheet, System.String _range)
            {
                System.String _valueCell = string.Empty;
                try
                {
                    Worksheet worksheet = _server.Worksheets[_nameSheet];
                    _valueCell = worksheet.Range[_range].Value.TextValue.ToString();
                }
                catch (Exception)
                {
                    throw;
                }
                return _valueCell;
            }

            public void SaveDocument(System.String _path)
            {
                try
                {
                    _server.SaveDocument(_path, DocumentFormat.Xlsx);

                }
                catch (Exception)
                {
                    throw;
                }
            }

            public void ExportPDFDocument(System.String _namePdf)
            {
                try
                {
                    _server.ExportToPdf(_namePdf);
                }
                catch (Exception)
                {
                    throw;
                }
            }


            public void ExportPDFSheet(System.String _namePdf)
            {
                Workbook _servertmp = null;
                try
                {
                    foreach (Worksheet _worksheet in _server.Worksheets)
                    {
                        _servertmp = new Workbook();
                        _servertmp.Worksheets.Add(_worksheet.Name);
                        _servertmp.Worksheets[_worksheet.Name].CopyFrom(_worksheet);
                        _servertmp.ExportToPdf(_namePdf.Replace(".pdf", string.Concat(System.DateTime.Now.Millisecond.ToString(), ".pdf")));
                        _servertmp.Dispose();
                    }
                }
                catch (System.Exception)
                {
                    throw;
                }
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            
            protected virtual void Dispose(bool disposing)
            {
                if (disposed)
                    return;
                if (disposing)
                {                    
                }                
                disposed = true;
            }
        }    
}
