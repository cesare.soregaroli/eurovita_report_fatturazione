﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fatturazione_DB;
using Fatturazione_Mail;

namespace Fatturazione_Cnsl
{
    class Program
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            System.String _NameFile = System.Configuration.ConfigurationManager.AppSettings["Name_Report"].ToString();
            System.String _NameFolder = System.Configuration.ConfigurationManager.AppSettings["Folder_Report"].ToString();
            System.String _NameFolderBackup = System.Configuration.ConfigurationManager.AppSettings["Folder_ReportBackup"].ToString();
            Int16 month_before = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["Month_Before"].ToString());
            try
            {
                var _dataTo = DateTime.Now.AddMonths(- month_before).Date;                
                Logger.Info("Start => Report Fatturazione");
                Fatturazione_DBLayer ofatturazionedb = new Fatturazione_DBLayer();
                ofatturazionedb.ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Eurovita_PA"].ConnectionString;
                Logger.Info($"Connection String => {ofatturazionedb.ConnString}");
                ofatturazionedb.fileName_In = _NameFile;
                Logger.Info($"Name File => {ofatturazionedb.fileName_In}");
                ofatturazionedb.Folder_Out = _NameFolder;
                Logger.Info($"Name Folder => {ofatturazionedb.Folder_Out}");
                ofatturazionedb.fileName_Out = String.Format(_NameFile, _dataTo.Month.ToString(), _dataTo.Year.ToString(), System.DateTime.Now.ToString("yyyy-MM-dd"));
                Logger.Info($"Name FileName Out => {ofatturazionedb.fileName_Out}");
                ofatturazionedb.Folder_Backup = _NameFolderBackup;
                //ofatturazionedb.Date_From = new DateTime(2019, 8, 1);
                ofatturazionedb.Date_From = new DateTime(_dataTo.Year, _dataTo.Month, 1);                
                Logger.Info($"Date From => {ofatturazionedb.Date_From.ToString()}");
                //ofatturazionedb.Date_To = new DateTime(2019, 8, 31);
                ofatturazionedb.Date_To = new DateTime(_dataTo.Year, _dataTo.Month, DateTime.DaysInMonth(_dataTo.Year,_dataTo.Month));                
                Logger.Info($"Date To => {ofatturazionedb.Date_To.ToString()}");
                ofatturazionedb.Create_Report_ToConsole();
                SendMailAttachment(ofatturazionedb.Folder_Out, ofatturazionedb.fileName_Out);
                Logger.Info("End => Report Fatturazione");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                if (ex.InnerException != null) { Logger.Error(ex, ex.InnerException.StackTrace); }
                Logger.Error(ex, ex.StackTrace);
                SendMail(ex);
            }
        }

        private static void SendMail(System.Exception ex)
        {
            Fatturazione_Mail.Fatturazione_Mail _fatturazioneMail = new Fatturazione_Mail.Fatturazione_Mail();
            Boolean _invioMailEccezione = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["email_eccezione"].ToString());
            try
            {
                Logger.Info("Start => SendMail");
                _fatturazioneMail.email_from = System.Configuration.ConfigurationManager.AppSettings["email_from"].ToString();
                _fatturazioneMail.email_to = System.Configuration.ConfigurationManager.AppSettings["email_to"].ToString();
                _fatturazioneMail.email_cc = System.Configuration.ConfigurationManager.AppSettings["email_cc"].ToString();
                _fatturazioneMail.email_sendemail = System.Configuration.ConfigurationManager.AppSettings["email_sendemail"].ToString();
                _fatturazioneMail.email_smtpserver = System.Configuration.ConfigurationManager.AppSettings["email_smtpserver"].ToString();
                _fatturazioneMail.email_eccezione_to = System.Configuration.ConfigurationManager.AppSettings["email_eccezione_to"].ToString();
                //invio mail a Microframe
                _fatturazioneMail.SendMailError(ex, true);
                if (_invioMailEccezione)
                {
                    //Invio Mail con stack dell'errore
                    _fatturazioneMail.SendMailError(ex, false);
                    Logger.Info("End => SendMail");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static void SendMailAttachment(System.String _folder, System.String _name)
        {
            Fatturazione_Mail.Fatturazione_Mail _fatturazioneMail = new Fatturazione_Mail.Fatturazione_Mail();
            Boolean _invioMailAttachment = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["email_reportFatturazione_enable"].ToString());
            try
            {
                Logger.Info("Start => SendMailAttachment");
                if (_invioMailAttachment)
                {
                    Logger.Info($"Invio Mail => {_invioMailAttachment}");
                    _fatturazioneMail.email_from = System.Configuration.ConfigurationManager.AppSettings["email_from"].ToString();
                    Logger.Info($"Mail FROM => {_fatturazioneMail.email_from}");
                    _fatturazioneMail.email_to = System.Configuration.ConfigurationManager.AppSettings["email_reportFatturazione_to"].ToString();
                    Logger.Info($"Mail To => {_fatturazioneMail.email_to}");
                    _fatturazioneMail.email_cc = System.Configuration.ConfigurationManager.AppSettings["email_reportFatturazione_cc"].ToString();
                    Logger.Info($"Mail CC => {_fatturazioneMail.email_cc}");                                        
                    _fatturazioneMail.email_smtpserver = System.Configuration.ConfigurationManager.AppSettings["email_smtpserver"].ToString();
                    Logger.Info($"Mail SMTP => {_fatturazioneMail.email_smtpserver}");
                    _fatturazioneMail.email_path_folder = _folder;
                    Logger.Info($"Mail email_path_folder => {_fatturazioneMail.email_path_folder}");
                    _fatturazioneMail.email_name_file = _name;
                    Logger.Info($"Mail email_name_file => {_fatturazioneMail.email_name_file}");
                    //Invio Mail l'allegato del file generato
                    _fatturazioneMail.SendMailAttachment();
                }
                Logger.Info("End => SendMailAttachment");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
